name := """LavaOne"""
organization := "com.crudApp"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.3"

libraryDependencies += jdbc
libraryDependencies += guice




transitiveClassifiers in Global := Seq()

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "8.0.18"
)
libraryDependencies ++= Seq(
  javaJpa,
  "org.hibernate" % "hibernate-core" % "5.4.9.Final"
)
libraryDependencies ++= Seq(
  "javax.ws.rs" % "javax.ws.rs-api" % "2.1.1"
)
libraryDependencies ++= Seq(
  "javax.annotation" % "javax.annotation-api" % "1.3.2",
  "org.jboss.resteasy" % "resteasy-jaxrs" % "3.0.2.Final",
  "org.glassfish.jersey.containers" % "jersey-container-servlet" % "2.29.1",
  "com.github.play2war" % "play2-war-core-servlet25_2.10" % "1.4-beta1"

)
