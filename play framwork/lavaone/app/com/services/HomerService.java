package com.services;

import com.models.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.repository.HomeRepository;

import java.util.List;

public class HomerService {

    /**
     * Create Logger and Repository Object
     **/
    private static final Logger logger = LoggerFactory.getLogger("services");
    private final static HomeRepository repo = new HomeRepository();

    /**
     * Declare flag
     **/
    private static int flag = 0;


    /**
     * GET All Movie Details
     **/
    public static List<Movie> getMovieDetails() {
        logger.info(" | Movie Service getMovieDetails Method |");
        List response = (List) repo.getMovieDetails();
        return response;
    }

    /**
     * POST Movie Details
     **/
    public static int postMovieDetails(Movie movie) {
        logger.info(" | Movie Service postMovieDetails Method |");
        if (movie.getSeats() > 10) {
            flag = 2;
        } else {
            flag = repo.postMovieDetails(movie);
        }
        return flag;

    }

    /**
     * Delete Movie Details
     **/
    public static int deleteMovieDetails(int id) {
        logger.info(" | Movie Service deleteMovieDetails Method |");
        flag = repo.deleteMovieDetails(id);
        return flag;
    }

    /**
     * Update Movie Details
     **/
    public static int updateMovieDetails(Movie movie, int id) {
        logger.info(" | Movie Service deleteMovieDetails Method |");
        flag = repo.updateMovieDetails(id,movie);
        return flag;
    }

    /**
     * GET All Movie Details By ID
     **/
    public static List<Movie> getMovieDetailsByID(int id) {
        logger.info(" | Movie Service getMovieDetailsByID Method |");
        List response = (List) repo.getMovieDetailsByID(id);
        return response;
    }
}
