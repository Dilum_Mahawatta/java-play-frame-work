package com.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Authentication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int iD;
    private String messageID;
    private String userName;
    private String key;
    private String sharedSecret;


    public int getiD() {
        return iD;
    }


    public void setiD(int iD) {
        this.iD = iD;
    }


    public String getSharedSecret() {
        return sharedSecret;
    }


    public void setSharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
    }


    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


}

