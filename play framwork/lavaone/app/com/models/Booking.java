package com.models;


import javax.persistence.*;

@Entity
public class Booking {

    // Declare Variables
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int bookingID;
    @Column
    private String userID;
    @Column
    private String movieID;
    @Column
    private int seats;
    @Column
    private String movieShowTime;
    @Column
    private int rSeats;


    //Declare Getters and Setters
    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public int getrSeats() {
        return rSeats;
    }

    public void setrSeats(int rSeats) {
        this.rSeats = rSeats;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getMovieID() {
        return movieID;
    }

    public void setMovieID(String movieID) {
        this.movieID = movieID;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getMovieShowTime() {
        return movieShowTime;
    }

    public void setMovieShowTime(String movieShowTime) {
        this.movieShowTime = movieShowTime;
    }


}
