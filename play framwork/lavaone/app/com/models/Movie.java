package com.models;


import javax.persistence.*;

@Entity
public class Movie{

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int movieID;
    @Column
    private String movieName;
    @Column
    private String movieDesc;
    @Column
    private String movieShowTime;
    @Column
    private int seats;

    public int getMovieID() {
        return movieID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieDesc() {
        return movieDesc;
    }

    public void setMovieDesc(String movieDesc) {
        this.movieDesc = movieDesc;
    }

    public String getMovieShowTime() {
        return movieShowTime;
    }

    public void setMovieShowTime(String movieShowTime) {
        this.movieShowTime = movieShowTime;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
}
