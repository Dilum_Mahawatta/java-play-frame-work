package com.repository;

import com.Utill.SessionUtil;
import com.models.Movie;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class HomeRepository {

    /**
     * Create Logger to Repository Class
     **/
    private static final Logger logger = LoggerFactory.getLogger("repository");


    /**
     * GET All Movie Details Method
     **/
    public Object getMovieDetails() {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        logger.info(" | Get Movie Details in Dao Layer |");
        List movies = new ArrayList<Movie>();

        try {

            movies = session.createQuery("SELECT m FROM Movie m").list();

        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return movies;
    }

    /**
     * POST Movie Details
     **/
    public int postMovieDetails(Movie movie) {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        logger.info(" | POST Movie Details in Dao Layer |");
        Movie movieDto = new Movie();
        int flag = 1;

        movieDto.setMovieName(movie.getMovieName());
        movieDto.setMovieDesc(movie.getMovieDesc());
        movieDto.setMovieShowTime(movie.getMovieShowTime());
        movieDto.setSeats(movie.getSeats());

        session.save(movieDto);

        tx.commit();
        session.close();

        return flag;

    }

    /**
     * Delete Movie Details
     **/
    public int deleteMovieDetails(int id) {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        logger.info(" | DELETE Movie Details in Dao Layer |");

        int flag = 0;
        Object rowCount = 0;
        try {

            String sql = "select max(b.bookingID) from Booking b where b.movieID='" + id + "'";
            Query query = session.createQuery(sql);
            System.out.println(query);
            rowCount = query.uniqueResult();
            System.out.println("Rows affected: " + rowCount);

            if (rowCount != null) {
                flag = 2;
            } else {
                String Sql = "delete from Movie where movieID =:mID";
                Query query2 = session.createQuery(Sql);
                query2.setInteger("mID", id);
                flag = query2.executeUpdate();

            }

        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            e.printStackTrace();
        } finally {
            tx.commit();
            session.close();
        }
        System.out.println(flag);
        return flag;
    }

    public int updateMovieDetails(int id, Movie movie) {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        logger.info(" | UPDATE Movie Details in Dao Layer |");

        int rowCount = 0;
        try {
            String sql = "update Movie set movieName = :mname, movieDesc=:mdesc, movieShowTime=:mtime, "
                    + "seats=:mseats where movieID = :mid";
            Query query = session.createQuery(sql);

            query.setInteger("mid", id);
            query.setString("mname", movie.getMovieName());
            query.setString("mdesc", movie.getMovieDesc());
            query.setString("mtime", movie.getMovieShowTime());
            query.setInteger("mseats", movie.getSeats());

            rowCount = query.executeUpdate();
            System.out.println("Rows affected: " + rowCount);

        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            e.printStackTrace();
            // TODO: handle exception
        } finally {
            tx.commit();
            session.close();
        }
        return rowCount;
    }

    /**
     * GET All Movie Details By ID Method
     **/
    public Object getMovieDetailsByID(int id) {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        logger.info(" | Get Movie Details By ID in Dao Layer |");
        List movies = new ArrayList<Movie>();

        try {

            movies = session.createQuery("SELECT m FROM Movie m WHERE m.seats != 0 AND m.movieID='"+id+"' ").list();

        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            e.printStackTrace();
        } finally {
            tx.commit();
            session.close();
        }

        return movies;

    }
}
