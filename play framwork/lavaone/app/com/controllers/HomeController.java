package com.controllers;

import com.Utill.ApplicationUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;
import play.mvc.*;
import com.services.HomerService;
import java.util.List;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    private static final Logger logger = LoggerFactory.getLogger("controllers");
    

    /**
     * Render Home HTML
     **/
    public Result index() {
        return ok(com.views.html.index.render());
    }

    /**
     * Get All Movie Details
     **/
    public Result getMovieDetails() {
        logger.info(" | Movie Controller getMovieDetails Method |");
        List<Movie> response = HomerService.getMovieDetails();
        if (response == null) {
            return notFound(ApplicationUtil.createResponse("Movies not found", false));
        } else {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonData = mapper.convertValue(response, JsonNode.class);
            return ok(ApplicationUtil.createResponse(jsonData, true));
        }

    }

    /**
     * POST Movie Details
     **/
    public Result postMovieDetails(Http.Request request) {
        logger.info(" | Movie Controller postMovieDetails Method |");
        JsonNode json = request.body().asJson();
        if (json == null) {
            return badRequest(ApplicationUtil.createResponse("Expecting JSON data", false));
        } else {
            int Results = HomerService.postMovieDetails(Json.fromJson(json, Movie.class));
            if (Results == 2) {
                return notFound(ApplicationUtil.createResponse("No of seats are Exceed", false));
            } else if (Results == 1) {
                return created(ApplicationUtil.createResponse("Successfully Inserted", true));
            } else {
                return notFound(ApplicationUtil.createResponse("Unsuccessful", false));
            }
        }
    }

    /**
     * DELETE Movie Details
     **/
    public Result deleteMovieDetails(int id) {
        logger.info(" | Movie Controller deleteMovieDetails Method |");
        int flag = HomerService.deleteMovieDetails(id);
        if (flag == 0) {
            return notFound(ApplicationUtil.createResponse("Movie with id :" + id + " not found", false));
        } else if (flag == 1) {
            return ok(ApplicationUtil.createResponse("Movie with id :" + id + " deleted", true));
        } else {
            return notFound(ApplicationUtil.createResponse("Movie with id :" + id + " Already Booked", false));
        }
    }

    /**
     * UPDATE Movie Details
     **/
    public Result updateMovieDetails(int id, Http.Request request) {
        logger.debug("In EmployeeController.update()");
        JsonNode json = request.body().asJson();
        if (json == null) {
            return badRequest(ApplicationUtil.createResponse("Expecting Json data", false));
        } else {
            int flag = HomerService.updateMovieDetails(Json.fromJson(json, Movie.class), id);
            if (flag == 1) {
                return ok(ApplicationUtil.createResponse("Movie with id :" + id + " successfully updated", true));
            } else {
                return notFound(ApplicationUtil.createResponse("Movie with id :" + id + " not found", false));
            }
        }
    }

    /**
     * GET Movie Details By ID
     **/
    public Result getMovieDetailsByID(int id){
        logger.info(" | Movie Controller getMovieDetailsByID Method |");
        List<Movie> response = HomerService.getMovieDetailsByID(id);
        if (response == null || response.isEmpty()) {
            return notFound(ApplicationUtil.createResponse("Movies not found", false));
        } else {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonData = mapper.convertValue(response, JsonNode.class);
            return ok(ApplicationUtil.createResponse(jsonData, true));
        }
    }


}
