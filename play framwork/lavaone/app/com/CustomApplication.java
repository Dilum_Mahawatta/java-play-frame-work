package com;

import com.Utill.AuthenticationFilter;
import org.glassfish.jersey.server.ResourceConfig;

public class CustomApplication extends ResourceConfig {

    public CustomApplication() {
        packages("com");

        // Register Auth Filter here
        register(AuthenticationFilter.class);
    }
}