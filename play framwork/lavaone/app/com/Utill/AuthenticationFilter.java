package com.Utill;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.jboss.resteasy.util.Base64;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Provider
public class AuthenticationFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    private static final String AUTHORIZATION_PROPERTY = "Authorization";

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Method method = resourceInfo.getResourceMethod();
        // Access allowed for all
        if (!method.isAnnotationPresent(PermitAll.class)) {
            // Access denied for all
            if (method.isAnnotationPresent(DenyAll.class)) {
                requestContext.abortWith(
                        Response.status(Response.Status.FORBIDDEN).entity("Access blocked for all users !!").build());
                return;
            }

            // Fetch authorization header
            final String authorization = requestContext.getHeaderString(AUTHORIZATION_PROPERTY);
            System.out.println("authorization: "+authorization);

            // Fetch MessageID
            final String messageID = requestContext.getHeaderString("messageid");
            System.out.println(messageID);

            // If no authorization information present; block access
            if (authorization == null || authorization.isEmpty() || messageID == null) {
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                        .entity("You cannot access this resource").build());
                return;

            } else {

                //Extract Authorization Header
                String[] parts = authorization.split(":");
                String userName = parts[0];
                String key = parts[1];
                System.out.println(userName + " " + key);

                // Retrieve SharedSecrete From Database
                Object sharedSecret = getAuthenticationDetails(messageID,userName,key);
                if (sharedSecret != null) {

                    String calculateHmacKey = calculateHmacKey(messageID, sharedSecret);
                    System.out.println(
                            "calculateHmacKey : " + calculateHmacKey + " headersDetails : " + key);

                    // Is user valid?
                    if(method.isAnnotationPresent(RolesAllowed.class)) {
                        RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
                        Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));
                        if (!isUserAllowed(calculateHmacKey, key, rolesSet)) {
                            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                                    .entity("You cannot access this resource").build());
                            return;
                        }
                    }


                }else {
                    requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                            .entity("You cannot access this resource").build());
                    return;
                }
            }

        } else {
            requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED).entity("You cannot access this resource").build());
            return;
        }


    }

    private boolean isUserAllowed(String calculateHmacKey, String key, Set<String> rolesSet) {
        // TODO Auto-generated method stub
        boolean isAllowed = false;

        if(key.contentEquals(calculateHmacKey)) {
            String userRole = "ADMIN";
            System.out.println(userRole);
            if(rolesSet.contains(userRole)) {
                isAllowed = true;
            }
        }else {
            isAllowed =false;
        }
        return isAllowed;
    }



    private String calculateHmacKey(String messageID, Object sharedSecret) {
        // TODO Auto-generated method stub
        byte[] HmacSHA1 = null;
        String HmacSHA2 = null;
        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec secretKeySpec = new SecretKeySpec(sharedSecret.toString().getBytes(), "HmacSHA1");
            mac.init(secretKeySpec);
            HmacSHA1 = mac.doFinal(messageID.getBytes());
            HmacSHA2 = Base64.encodeBytes(HmacSHA1);

        } catch (Exception e) {
            throw new RuntimeException("Failed to calculate hmac-sha256", e);
        }
        return HmacSHA2;
    }

    private Object getAuthenticationDetails(String messageID,String userName, String key) {
        // TODO Auto-generated method stub
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        Object sharedSecret = null;
        try {

            String sql = "select sharedSecret from Authentication where userName='" + userName + "'";
            Query query = session.createQuery(sql);
            System.out.println(query);
            // query.setInteger("mID", movieID);

            sharedSecret = query.uniqueResult();
            System.out.println("sharedSecret: " + sharedSecret);

        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            e.printStackTrace();
        } finally {
            tx.commit();
            session.close();
        }
        return sharedSecret;
    }
}
